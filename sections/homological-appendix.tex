
\section{Homological Algebra}
\subsection*{Quasi-Isomorphism vs. chain homotopy equivalence}
\begin{defn}
  Let $\chainc$ be a chain complex.
  \begin{enumerate}
    \item We say that $\chainc$ is \emph{acyclic} or \emph{exact} if all homology groups vanish; that $\chainc$ is \emph{contractible} if the identity map of $\chainc$ is null-homotopic.
  \end{enumerate}
  Let now $f\mc \chainc \to \chaind$ be a chain map.
  \begin{enumerate}[resume]
    \item We say that $f$ is a \emph{quasi-isomorphism} if $f$ induces isomorphisms on all homology groups, i.e. for all $n$, the map
    \[
    \hlgy_n(f)\mc \hlgy_n(C)\to \hlgy_n(D)
    \]
    is an isomorphism.
    \item We say that $f$ is a \emph{homotopy equivalence} if there is a homotopy inverse for $f$, i.e. a chain map $g\mc \chaind\to\chainc$ such that the compositions satisfy $f\circ g \simeq \id_{\chaind}$ and $g\circ f \simeq \id_{\chainc}$.
  \end{enumerate}
\end{defn}

\begin{numtext}
We first note that homotopy equivalences are quasi-isomorphisms -- the homotopy inverse is automatically an inverse on homologies.
The converse is however not true in general, as can be seen in the following examples:
In the lecture, we saw this for the morphism of chain complexes indicated in the diagram
\[
\begin{tikzcd}
  \ldots
  \ar{r}
  &
  0
  \ar{r}
  \ar{d}
  &
  \zz
  \ar{r}[above]{\cdot 2}
  \ar{d}
  &
  \zz
  \ar{r}
  \ar[twoheadrightarrow]{d}
  &
  0
  \ar{d}
  \ar{r}
  &
  \ldots
  \\
  \ldots
  \ar{r}
  &
  0
  \ar{r}
  &
  0
  \ar{r}
  &
  \zz/2\zz
  \ar{r}
  &
  0
  \ar{r}
  &
  \ldots
\end{tikzcd}
\]
The only non-trivial homology group of both complexes is $\zz/2\zz$ and the morphism above induces the identity on homology. However, it is not a weak equivalence, since the only chain map (we don't even need compatibility with the differentials) in the other direction is the trivial map, which automatically induces the zero map on homology. \par
Things can go wrong even when we restrict ourselves to acyclic complexes -- consider the chain complex $\chainc$ given by
\[
\begin{tikzcd}
\ldots
\ar{r}
&
\zz/4\zz
\ar{r}[above]{\cdot 2}
&
\zz/4\zz
\ar{r}[above]{\cdot 2}
&
\zz/4\zz
\ar{r}
&
\ldots
\end{tikzcd}
\]
which is exact. If we denote for the moment the zero-complex by $\chaind$, then the zero map $\chainc\to\chaind$ becomes a quasi-isomorphism, but it's not a  homotopy equivalence -- in this case, this would amount to the identity $\id_C$ being null-homotopic. That this is not the case can be seen in several ways: First of all, we know that $\hom(\zz/4\zz,\zz/4\zz) \cong \zz/4\zz$ (every linear map is given by multiplying with one of $0,\ldots,4$). One can now check that none of the possible choices for null-homotopies would work. More conceptually, we note that any null-homotopy $s$ of the identity map
\[
\begin{tikzcd}
\ldots
\ar{r}
&
\zz/4\zz
\ar{r}[above]{\cdot 2}[below]{f_{n+1}}
&
\zz/4\zz
\ar{r}[above]{\cdot 2}[below]{f_n}
\ar[bend right =35, dashed]{l}[above]{s_n}
&
\zz/4\zz
\ar[bend right=35, dashed]{l}[above]{s_{n+1}}
\ar{r}
&
\ldots
\end{tikzcd}
\]
would give in particular rise to maps $s_{n}\mc \zker_{n}(C)\to C_{n+1}$. Now for an element $x\in \zker_{n+1}(C)$, we have $s_{n-1}d_n(x) = x$. Note also that by exactness, the differential restricts to surjective maps $d_n\mc C_{n+1}\twoheadrightarrow \zker_n(C)$. Put together, we see that the null-homotopy gives  splitings in the short-exact sequences of the form
\[
0
\to
\zker_n(C)
\hookrightarrow
C_n
\xrightarrow{d_n}
\zker_{n-1}(C)
\to
0
\]
In our case, this would mean that the sequence
\[
0
\to
\zz/2\zz
\hookrightarrow
\zz/4\zz
\xrightarrow{\cdot 2}
\zz/2\zz
\to
0
\]
splits. But this would mean $\zz/4\zz\cong \zz/2\zz\oplus \zz/2\zz$, which is false. \par
The above reasoning can be promoted to the following lemma:
\end{numtext}
\begin{lem}
  A chain complex $\chainc$ is contractible if and only if it is acyclic and for every $n$, the sequence
  \[
  0
  \to
  \zker_{n+1}(C)
  \hookrightarrow
  C_{n+1}
  \to
  \zker_n(C)
  \to
  0
  \]
  splits.
\end{lem}
\begin{proof}
  We have already shown one direction, so let's assume that $\chainc$ is acyclic and each one of these sequences splits, say by maps $s_n\mc \zker_n(C)\to C_{n+1}$.
  Then every $C_n$ decomposes as $C_{n+1}= \zker_{n+1}(C)\oplus \im s_n$ and the maps \[h_{n+1}\mc C_{n+1}\to C_{n+2}\] with
  \[\restrict{h_{n+1}}{\zker_{n+1}(C)} = s_{n+1} \text{ and }\restrict{h}{\im s_n}=0\]
 yield a null-homotopy of the identity.
\end{proof}
We also have the following positivity results:
\begin{prop}\label{app:quasi-iso-implies-homotopy-equivalence-for-free}
Let $f\mc \chainc\to\chaind$ be a quasi isomorphism of chain complexes (over an arbitrary commutative ring $k$).
\begin{enumerate}
  \item If both $\chainc$ and $\chaind$ are bounded and consist only of projective modules, then $f$ is a homotopy equivalence.
  \item Assume now that both $\chainc$ and $\chaind$ consist only of free abelian groups, but are not necessarily bounded. Then $f$ is also a homotopy equivalence.
\end{enumerate}
\end{prop}
