\section{(Singular) Cohomology}
\begin{numtext}
  In the last semester, we studied the following functorial constructions to understand topological spaces by algebraic means:
  \[
  \begin{tikzcd}[column sep = huge]
    \topcat
    \ar{r}[above]{\sing}[below]{\substack{\text{simplicial} \\\text{complex}}}
    &
    \sset
    \ar{r}[above]{\linearization(-;A)}[below]{\text{linearization}}
    &
    \chaincat(A)
    \ar{r}[above]{\hlgy_n(-)}[below]{\substack{\text{homology}\\\text{group}}}
    &
    \abcat
  \end{tikzcd}
  \]
  \glsadd{singular-homology}
  To make more sense of this, let us briefly recall what these symbols and constructions are supposed to mean and do:
  \begin{itemize}
    \item For a topologicial space $X$, the \emph{singular complex} $\sing(X)$ is the simplicial set whose $n$-simplices are given by the set of continous maps from the topological $n$-simplex $\tsimplex^n$ to $X$:
    \[
    \sing(X)_n = \topcat(\tsimplex^n,X)
    \]
    \glsadd{topological-simplex}\glsadd{simplicial-complex}
    Recall that the \emph{topological $n$-simplex} is the geometric realization of the standard $n$-simplex $\Delta^n$ -- or, concretely, given by
    \[
    \tsimplex^n =
    \lset
    \left(x_0,\ldots,x_n\right)\in \rr^{n+1}\ssp
    x_i\geq 0,x_0+\ldots+x_n = 1
    \rset
    \]
    The functor $\sing$ is part of the adjunction
    \[
    \left|-\right|\mc
    \sset
    \leftrightarrows
    \topcat
    \mcc
    \sing
    \]

    \item For a simplicial set $S$ and an abelian group $A$, the \emph{linearization of $S$} with coefficients in $A$ is the chain complex whose $n$-th ($n\geq 0$) group is given by the direct sum of $A$ index by the $n$-simplices of $S$:
    \[
    \linearization(S;A) \defined A^{\oplus S_n}
    \]
    \glsadd{linearization}
    The boundary operators are given by
    \[
    d_n \mc \linearization(S;A)_n \to \linearization(S;A)_{n-1},~
    x \mapsto \sum_{i=0}^n(-1)^id_i^\ast,
    \]
    where the linear map $d_i^\ast\mc\linearization(S;A)_n\to \linearization(S;A)_{n-1}$ is extended from the maps $d_i^\ast\mc S_n\to S_{n-1}$ on basis elements (notice the clash of notation), which in turn are induced by the ``canonical'' degeneracy maps $d_i\mc\mc [n-1]\to [n]$ (they ommit $i$ in the image).\glsadd{simplicial-degeneracy}
    \item For a chain complex of abelian groups $\chainc$ and $n\in \zz$, the \emph{$n$-th homology group} is given by the quotient
    \[
    \hlgy(C)_n\defined
    \ker(C_n\xrightarrow{d_n}C_{n-1})
    /
    \im(C_{n+1}\xrightarrow{d_{n+1}}C_n).
    \]
    \glsadd{homology}
  \end{itemize}
  We now want to dualize this construction, which will lead, somewhat surprisingly, to more structure.\footnote{\emph{Author's note:} This is all very nebolous at the moment. Hopefully, I'll find the time to add more details and maybe even a somewhat meaningful outline later on.}
\end{numtext}

\begin{defn}
\leavevmode
\begin{enumerate}
\item
  A \emph{cochain complex} $\cchainc$ consists of a collection of abelian groups $(C^n)_{n\in\zz}$ and homomorphisms $d^n\mc C^n\to C^{n+1}$ such that ``$d^2 = 0$'', i.e. for all integers $n$ the composition
  \[
  C^n
  \xrightarrow{d^n}
  C^{n+1}
  \xrightarrow{d^{n+1}}
  C^{n+2}
  \]
  is the zero-map.
  \index{complex!cochain}
\end{enumerate}
This is remarkably similar to the definition of a chain complex (see \cref{cochain-chain-duality}). We now want to carry over all the notions we know from chain complexes to \emph{co}-chain complexes.
\begin{enumerate}[resume]
\item A \emph{morphism of chain complexes} or \emph{cochain map} $f\mc \cchainc\to\cchaind$ is given by a collection of linear maps $f^n\mc C^n\to D^n$ such that ``$d\circ f = f\circ d$'',
 i.e. for all $n\in \zz$, the diagram
 \[
 \begin{tikzcd}
   C^n
   \ar{r}[above]{d^n_C}
   \ar{d}[left]{f^n}
   &
   C^{n+1}
   \ar{d}[right]{f^{n+1}}
   \\
   D^n
   \ar{r}[below]{d^n_D}
   &
   D^{n+1}
 \end{tikzcd}
 \]
 commutes.
 \item The \emph{$n$-th cohomology group} of a cochain complex $\cchainc$ is given by the quotient
 \[
 \hlgy^n(C) =
 \ker(C^n\xrightarrow{d^n}C^{n+1})
 /
 \im(C^{n-1}\xrightarrow{d^{n-1}}C^n)
 \]
 \item A \emph{cochain homotopy} between cochain maps $f,g\mc \cchainc \to \cchaind$ is given by a collection of linear maps $s^n\mc C^n\to D^n$ such that ``$d\circ s + s \circ d = f -g$'', i.e. for all $n\in \zz$ the identity
 \[
 d^{n-1}\circ s^n + s^{n+1} \circ d^n = f^n-g^n
 \]
 holds in the diagram
 \[
  \begin{tikzcd}
    C^{n-1}
    \ar{r}[above]{d^{n-1}}
    &
    C^n
    \ar{r}[above]{d^n}
    \ar[shift left]{d}
    \ar[shift right]{d}
    \ar{ld}[above left]{s^n}
    &
    C^{n+1}
    \ar{ld}[above left]{s^{n+1}}
    \\
    D^{n-1}
    \ar{r}[above]{d^{n-1}}
    &
    D^n
    \ar{r}[below]{d^n}
    &
    D^{n+1}
  \end{tikzcd}.
  \]
\end{enumerate}
\end{defn}
\begin{construction}
  All the main tools and property from chain complexes carry over to cochain complexes:
  \begin{enumerate}
    \item A cochain map $f\mc \cchainc\to \cchaind$ induces maps on all cohomology groups:
    \[
    \hlgy^n(f)\mc \hlgy^n(C) \to \hlgy^n(D),~
    [x] \mapsto [f^n(x)]
    \]
    \glsadd{induced-map-on-cohomology}
    If two cochain maps $f,g\mc \cchainc\to \cchaind$ are homotopic, then their induced maps on cohomology agree.
    \item For every short exact sequence of cochain-complexes
    \begin{equation}\label{singularcohomology:generic-ses}\tag{$\ast$}
    \begin{tikzcd}[column sep = small]
      0
      \ar{r}
      &
      \cchain{A}
      \ar{r}[above]{f}
      &
      \cchain{B}
      \ar{r}[above]{g}
      &
      \cchain{C}
      \ar{r}
      &
      0
    \end{tikzcd}
    \end{equation}
    there is an induced natural long-exact sequence of cohomology groups:
    \[
    \begin{tikzcd}
      \ldots
      \ar{r}
      &
      \hlgy^n(B)
      \ar{r}[above]{\hlgy^n(g)}
      \ar[phantom]{d}[name=X, anchor = center]{}
      &
      \hlgy^n(C)
      \ar[rounded corners,
            to path={ -- ([xshift=2ex]\tikztostart.east)
                      |- (X.center) \tikztonodes
                      -| ([xshift=-2ex]\tikztotarget.west)
                      -- (\tikztotarget)}]{dll}[at end]{\partial^n}
      \\
      \hlgy^{n+1}(A)
      \ar{r}[below]{\hlgy^{n+1}(f)}
      &
      \hlgy^{n+1}(B)
      \ar{r}
      &
      \ldots
    \end{tikzcd}
    \]
    Explicitly, the \emph{connecting homomorphism} $\partial^n$ is constructed as follows: Given a $x\in \zker^n(C)$, choose an element $\wtilde{x}\in B^n$ such that $g^n(\wtilde{x}) = x$ (this works, since \eqref{singularcohomology:generic-ses} being short-exact implies in particular that all $g^n$ are surjective).
     Then, since $g$ is a cochain map, we have
    \begin{align*}
      g^{n+1}(d_B^n(\wtilde{x}))
      &=
      d_C^n(g^n(\wtilde{x}))\\
      &=
      d^n_C(x)\\
      &=
      0,
    \end{align*}
    so there is a $y\in A^{n+1}$ such that $f^{n+1}(y) = d^{n+1}_B(\wtilde{x})$. This gives the desired image of $[x]$ under $\partial^n$.\par
    The construction of ``taking-a-long-exact-cohomology-sequence'' is also \emph{natural} in the following sense: Given a short-exact sequence of cochain complexes \eqref{singularcohomology:generic-ses} and another one
    \begin{equation}\label{singularcohomology:modified-ses-1}
    \tag{$\wtilde{\ast}$}
      \begin{tikzcd}
      0
      \ar{r}
      &
      \cchain{\wtilde{A}}
      \ar{r}[above]{\wtilde{f}}
      &
      \cchain{\wtilde{B}}
      \ar{r}[above]{\wtilde{g}}
      &
      \cchain{\wtilde{C}}
      \ar{r}
      &
      0
      \end{tikzcd}
    \end{equation}
    which fit together in a commutative diagram of the form
    \[
    \begin{tikzcd}
      0
      \ar{r}
      &
      \cchain{A}
      \ar{d}[left]{\alpha}
      \ar{r}[above]{f}
      &
      \cchain{B}
      \ar{d}
      \ar{r}[above]{g}
      &
      \cchain{C}
      \ar{d}[right]{\gamma}
      \ar{r}
      &
      0
      \\
      0
      \ar{r}
      &
      \cchain{\wtilde{A}}
      \ar{r}[below]{\wtilde{f}}
      &
      \cchain{\wtilde{B}}
      \ar{r}[below]{\wtilde{g}}
      &
      \cchain{\wtilde{C}}
      \ar{r}
      &
      0
    \end{tikzcd},
    \]
    then the connecting homomorphisms $\partial$ and $\wtilde{\partial}$ associated to \eqref{singularcohomology:generic-ses} and \eqref{singularcohomology:modified-ses-1} fit into commutative diagrams of the form
    \[
    \begin{tikzcd}
    \hlgy^n(C)
    \ar{r}[above]{\partial^n}
    \ar{d}[left]{\hlgy^n(\gamma)}
    &
    \hlgy^{n+1}(A)
    \ar{d}[right]{\hlgy^{n+1}(\gamma)}
    \\
    \hlgy^n(\wtilde{C})
    \ar{r}[below]{\wtilde{\partial}^n}
    &
    \hlgy^{n+1}(\wtilde{A})
    \end{tikzcd}.
    \]
  \end{enumerate}
\end{construction}


\begin{rem}\label{cochain-chain-duality}
Both the collection of (co-) chain complex together with (co-)chain maps of abelian groups are abelian categories, denoted by $\chaincat(\zz)$ and $\cchaincat(\zz)$ respectivley. These two categories are dual in a very strict sense: We can define a functor $D\mc \chaincat(\ccat)\to\cchaincat(\zz)$ by defining the cochain complex $\cchain{DC}$ as $\left(\cchain{DC}\right)^n\defined C_{-n}$ with differentials $d^n_{DC}\defined d^C_{-n}$.
On morphisms, for a chain map $f\mc \chainc\to\chaind$, we set $(Df)^n\defined f_{-n}$. This process is strictly invertible, and not just up to a natural isomorphism! Moreover, it holds that $\hlgy^n(DC) = \hlgy_{-n}(C)$ and two chain maps $f,g\mc \chainc\to \chaind$ are homotopic if and only if the cochain maps $Df,Dg\mc \cchainc\to\cchaind$ are homotopic.
\end{rem}

\begin{construction}
  Let $\chainc$ be a chain complex and $A$ an abelian group.
  We define the \emph{dual complex of $\chainc$ with coefficients in $A$}, $\hom(C,A)$, to be the cochain complex with modules $\hom(C,A)^n\defined\hom(C_n,A)$
  and differentials
  $d^n\mc \hom(C,A)^n\to \hom(C,A)^{n+1}$ given by the precomposition-maps
  \begin{align*}
  \dual{d_{n+1}}{A}\mc \hom(C^n,A)&\to \hom(C^{n+1},A)\\
  (C^n\xrightarrow{f}A)&\mapsto
  (C^{n+1}\xrightarrow{d_{n+1}}C^n\xrightarrow{f}A)
  \end{align*}
  We can upgrade this construction to a contravariant functor
  \[
  \hom(-,A)\mc
  \chaincat(\zz)\op \to \cchaincat(\zz)
  \]
  by assigning to a chain map $f\mc \chainc\to \chaind$ the map
  \[
  \hom(f,A)\mc \hom(D,A)\to\hom(C,A)
  \]
  which has as $n$-th component again the precomposition map
  \begin{align*}
    \dual{f_n}{A}\mc
    \hom(D_n,A)&\to \hom(C_n,A)\\
    (D_n\xrightarrow{h}A)&\mapsto
    (C_n\xrightarrow{f_n}D_n\xrightarrow{h}A)
  \end{align*}
\end{construction}
%slogan: Dualizing presevers homotopy
\begin{lem}\label{singularcohomology:dualizing-presevers-he}
  Let $f,g\mc \chainc\to\chaind$ be homotopic chain maps. Then the dual maps
  \[
  \hom(f,A),\hom(g,A)\mc
  \hom(D,A)\to\hom(C,A)
  \]
  are again homotopic. In particular, if $f\mc \chainc\to \chaind$ is a homotopy equivalence, $\hom(f,A)$ is too.
\end{lem}
\begin{mrem}
  The analogous statement is in general \emph{mucho} false for homology -- dualizing does not presever homology!
  For example, we know that the hom-functor $\hom(-,\zz/2\zz)$ is not exact: Given the short-exact sequence
  \[
  \begin{tikzcd}[column sep = small]
  0
  \ar{r}
  &
  \zz
  \ar{r}[above]{\cdot 2}
  &
  \zz
  \ar[twoheadrightarrow]{r}
  &
  \zz/2\zz
  \end{tikzcd},
  \]
  applying $\hom(-,\zz/2\zz)$ yields the (no longer exact) complex
  \[
  \begin{tikzcd}[column sep = small]
  0
  \ar{r}
  &
  \hom(\zz/2\zz,\zz/2\zz)
  \ar{r}
  \ar[equal]{d}
  &
  \hom(\zz,\zz/2\zz)
  \ar[twoheadrightarrow]{r}
  \ar[equal]{d}
  &
  \hom(\zz,\zz/2\zz)
  \ar[equal]{d}
  \ar{r}
  &
  0
  \\
  0
  \ar{r}
  &
  \zz/2\zz
  \ar{r}
  &
  0
  \ar{r}
  &
  0
  \ar{r}
  &
  0
  \end{tikzcd},
  \]
  which no longer has trivial cohomology.
\end{mrem}

\subsection{The singular cohomology of spaces and simplicial sets}
\begin{defn}
  Let $S$ be a simplicial set and $A$ an abelian group. The \emph{cohomology of $S$ with coefficients in $A$} is defined as:
  \[
  \hlgy^n(Y;A)\defined
  \hlgy^n\left(
  \hom(\linearization(Y;\zz),A)
  \right)
  \]
  \glsadd{singular-cohomology}
  If $S'\sse S$ is a simplicial subset, then the \emph{relative singular cohomology} of the pair $(S,S')$ is given by
  \[
  \hlgy^n(S,S';A)\defined
  \hlgy^n\left(
  \hom(\linearization(S,S');\zz),A
  \right)
  \]
  Here, $\linearization(S,S';\zz)$ denotes the quotient complex $\linearization(S;\zz)/\linearization(S';\zz)$.\par
  If $X$ is a topological space, then the \emph{singular cohomology} of $X$ is given by the singular cohomology of the associated simplicial set:
  \[
  \hlgy^n(X;A)\defined \hlgy^n(\sing(X);A).
  \]
  \glsadd{singular-cohomology}
  If $X'\sse X$ is a subspace, then the \emph{relative singular cohomology} of the pair $(X,X')$ is given by the relative cohomology of the associated simplicial sets:
  \[
  \hlgy^n(X,X';A)\defined \hlgy^n(\sing(X),\sing(X');A)
  \]
  \end{defn}
There is an alternative construction for singular cohomology, by going the dual way:
\begin{construction}
  Let $S$ be a simplicial set and $A$ an abelian group. We define the cochain complex $\linearization^\ast(S;A)$ consisting of abelian groups
  \[
  \linearization^n(S;A)
  \defined
  \setcat(S_n,A)
  \]
  and differentials
  \[
  d^n\mc \setcat(S_n,A) \to \setcat(S_{n+1},A)
  \]
  defined by
  \[
  d^n(f)(y)\defined
  \sum_{i=0}^{n+1}\left(-1\right)^if(d_i^\ast(y)).
  \]
  Note that the group structure on $\setcat(S_n,A)$ is inherited by the group structure on $A$.
  %We quickly check that this is indeed a cochain complex:
  %For a map $S_n\xrightarrow{f}A\in \linearization^n(S;A)$ and an element $y\in S_{n+1}$ we have
  %\begin{align*}
  %  d^{n+1}\left(d^n(f)\right)(y)&=
  %  \sum_{i=0}^{n+2}
  %  \left(-1\right)^i d^n(f)(d_i^\ast(y))
  %  \\
  %  &=
  %  \sum_{i=0}^{n+2}
  %  \left(-1\right)^i
  %  \sum_{j=0}^{n+1}
  %  \left(-1\right)^j f\left(d_j^\ast d_i^\ast(y)\right)\\
  %  &= 0
  %\end{align*}
  %where the last line should follow from the standard argument why such sums are zero.\par
  If $S'\sse S$ is a simplicial subset, then we define a relative version of the above as
  \[
  \linearization^{\ast}(S,S';A)
  \defined
  \lset f\mc Y_n\to A\ssp \restrict{f}{Y'_n} = 0
  \rset
  \]
  One can then check that this defines a sub-complex of $\linearization^\ast(S;A)$.
\end{construction}
\begin{prop}
  Let $S$ be a simplicial set and $A$ an abelian group. Then there is an isomorphism of cochain complexes
  \[
  \hom(\linearization(S;\zz),A)
  \xrightarrow{\sim}
  \linearization^\ast(S;A).
  \]
  Induced from this, we get isomorphisms of cohomology groups
  \[
  \hlgy^n(S;A)
  \xrightarrow{\sim}
  \hlgy^n(\linearization^\ast(S;A)).
  \]
  These isomorphisms are natural for morphisms of simplicial sets.\par
  For a simplicial subset $S'\sse S$, there is an isomorphism of cochain complexes
  \[
  \hom(\linearization(S,S';\zz),A)
  \xrightarrow{\sim}
  \linearization^{\ast}(S,S';A),
  \]
  which is again natural for morphisms of pairs of simplicial sets.
\end{prop}
\begin{proof}
  We only prove the absolute case:
  For that, we need maps
  \[
  \pphi^n\mc
  \hom(\linearization(Y;Z),A)
  =
  \hom(\zz^{\oplus Y_n},A)
  \xrightarrow{\sim} \setcat(Y_n,A).
  \]
  But we already know a canonical choice for that -- the natural isomorphism from the free-forgetful-adjunction:
  \[
  \zz^{\oplus(-)}\mc \abcat \leftrightarrows \setcat\mcc \mathrm{Forget},
  \]
  given in the other direction linearly extending from linearly extending the images of the free generators.\par
  Now, we still need to check that these maps are compatible with the differentials, and natural. That they are compatible with the differentials follows directly from the construction of the differentials on the two cochain complexes (we do basically nothing but evaluate morphisms). The naturality of the isomorphisms should follow from the naturality of the adjunction.
\end{proof}

\begin{numtext}
  One of the nice properties of singular cohomology is that most of the theorems on singular homology cary over verbatim. We list a few:
  \begin{enumerate}
    \item \emph{Homotopy Invariance}: If $f,g\mc X\to Y$ are homotopic continous maps, then they induce the same maps on cohomology, i.e. for all $n\geq 0$, the maps
    \[
    \hlgy^n(f),\hlgy^n(g)
    \mc \hlgy^n(Y)\to\hlgy^n(X)
    \]
    are equal.
    \item \emph{Long exact sequence}:
    If $S'\sse S$ is a simplicial subset, then there is a short exact of cochain complexes
    \[
    \begin{tikzcd}[column sep = small]
      0
      \ar{r}
      &
      \linearization^{\ast}(S,S';A)
      \ar{r}
      &
      \linearization^{\ast}(S;A)
      \ar{r}
      &
      \linearization
      \ar{r}
      &
      0
    \end{tikzcd},
    \]
    where the rightmost map is giving by restriction. Again, this induces a long exact cohomology sequence of the form
    \[
    \begin{tikzcd}
      \ldots
      \ar{r}
      &
      \hlgy^n(S;A)
      \ar[phantom]{d}[name=X, anchor = center]{}
      \ar{r}
      &
      \hlgy^n(S';A)
      \ar[rounded corners,
            to path={ -- ([xshift=2ex]\tikztostart.east)
                      |- (X.center) \tikztonodes
                      -| ([xshift=-2ex]\tikztotarget.west)
                      -- (\tikztotarget)}]{dll}[at end]{\partial^n}
      \\
      \hlgy^{n+1}(S')
      \ar{r}
      &
      \hlgy^{n+1}(S,S')
      \ar{r}
      &
      \ldots
    \end{tikzcd}
    \]
    and this assignement also natural.
    For a subspace $X'\sse X$, we can apply this construction to get the \emph{long exact cohomology sequence of the pair $(X,X')$}.
    \item \emph{Excision}: Let $(X,Y,U)$ be an excisive tripple of spaces, i.e. subspaces $U\sse Y\sse X$ and $\clos(U)\sse\interior(Y)$.
    In the proof of the excision theorem, we showed that the inclusions induce a quasi-isomorphism of chain complexes
    \[
    i\mc \linearization(\sing(X\setminus U;Y\setminus U);\zz)
    \to
    \linearization(\sing(X),\sing(Y);\zz),
    \]
    so in particular isomorphisms on all homology groups.
    The fact that the notions of ``quasi-isomorphism'' and ``chain homotopy equivalence'' agree for chain complexes of abelian groups (\cref{app:quasi-iso-implies-homotopy-equivalence-for-free}) together with \cref{singularcohomology:dualizing-presevers-he} implies that
    \[
    \hom(i,A)\mc
    \hom(\linearization(\sing(X),\sing(Y);\zz),A)
    \to
    \hom(\linearization(X\setminus U,Y\setminus U);A),\zz)
    \]
    is again a cochain homotopy equivalence. So $\hom(i,A)$ induces isomorphisms of cohomology groups too.
    \item Given all we know so far about singular cohomology, we can confidently state that all the basic calculations for homology involving only these facts should cary over in essentially in the same way to cohomology. In particular, we have for the spheres $\sphere^n$ and discs $\dd^n$ (and $n\geq 1$):
    \begin{align*}
    &\hlgy^m(\sphere^n;A)
    \cong
    \begin{cases}
      A&m=0,n\\
      0&\text{otherwise}
    \end{cases}\\
    &\hlgy^m(\dd^n,\sphere^{n-1};A)
    \cong
    \begin{cases}
      A&m=n\\
      0&\text{otherwise}
    \end{cases}
    \end{align*}
    \glsadd{sphere}
    \glsadd{disk}
  \end{enumerate}
\end{numtext}
\begin{recall}
  Let $X$ be an absolute CW-complex with skeleton $\lset X_n\rset_{n\geq 0}$. Then the \emph{cellular chain complex} of $X$ is given by
  \[
  \cellularc(X;A) \defined \hlgy_n(X_n,X_{n-1};\zz),
  \]
  with \emph{cellular differentials} defined as the composition
  \[
  \hlgy_n(X_n,X_{n-1};\zz)
  \xrightarrow{\partial}
  \hlgy_{n-1}(X_{n-1};\zz)
  \to
  \hlgy_{n-1}(X_{n-1},X_{n-2};\zz)
  \]
  \glsadd{cellular-chain-complex}
  We know that singular homology and cellular homology agree.
\end{recall}
\begin{defn}
  In this case, we define the \emph{cellular cochain complex} of $X$ with coefficients in $A$ as
  \[
  \cellularcc(X;A)\defined
  \hom(\cellularc(X;\zz),A)
  \]
  \glsadd{cellular-cochain-complex}
\end{defn}
\begin{prop}
  Cellular cohomology and singular cohomology are again isomorphic, i.e. for all $n\geq 0$ there are isomorphisms
  \[
  \hlgy^n(\cellularcc(X;A))
  \cong
  \hlgy(X;A)
  \]
  which are natural for cellular maps.
\end{prop}
\begin{example}
  \leavevmode
  \begin{enumerate}
    \item Let $X$ be a CW-complex with no cells in odd dimensions. Then $\cellularc(X;\zz)$ has trivial differentials, and so does \[\hom(\cellularc(X;Z),A) = \cellularcc(X,A).\] This means that the cohomology is given by
    \begin{align*}
      \hlgy^n(X;A)
      &\cong
      \hlgy^n(\cellularcc(X,A))
      \\
      &=
      \hom(\cellularcc_n(X,\zz),A)
      \\
      &\cong
      \hom(\zz^{\bigoplus I_n},A)
      \\
      &\cong
      \setcat(I_n,A),
    \end{align*}
    where $I_n$ denotes the set of $n$-cells of $X$.
    \item As a particular instance of i), we consider $\cc\pp^{\infty}$, which has a CW-structure with exactly 1 cell in every dimension. Then the cohomology is given by
    \[
    \hlgy^n(\cc\pp^\infty;A)
    \cong
    \begin{cases}
      A&n \text{ even}\\
      0&n \text{ odd}
    \end{cases}
    \]
  \end{enumerate}
\end{example}
